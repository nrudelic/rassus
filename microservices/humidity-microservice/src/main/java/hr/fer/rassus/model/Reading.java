package hr.fer.rassus.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Reading {
	
	@Id
	private Integer ID;
	private Double temperature;
	private Double humidity;
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public Double getHumidity() {
		return humidity;
	}
	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}
	
	
}
