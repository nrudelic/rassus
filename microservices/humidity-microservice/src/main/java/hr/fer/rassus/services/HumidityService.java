package hr.fer.rassus.services;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import hr.fer.rassus.model.Reading;
import hr.fer.rassus.repository.ReadingRepository;

@Service
public class HumidityService {
	@Autowired
	private ReadingRepository readingRepository;

	public Reading getReading() {
		LocalTime time = LocalTime.now();
		int id = (int) (4 * time.getHour() + time.getMinute() / 15);
		return readingRepository.findById(id).get();
	}
}
