package hr.fer.rassus.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import hr.fer.rassus.model.Reading;
import hr.fer.rassus.services.HumidityService;


@RestController
public class Controller {
	
	private HumidityService humidityService;
	
	public Controller(HumidityService service) {
		humidityService = service;
	}
	
	@GetMapping("/current-reading")
	public ResponseEntity<Double> currentReading(){
		Double humidity = humidityService.getReading().getHumidity();
		System.out.println(humidity);
		return new ResponseEntity<Double>(humidity, HttpStatus.OK);
	}
}