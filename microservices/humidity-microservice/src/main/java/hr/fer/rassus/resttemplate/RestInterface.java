package hr.fer.rassus.resttemplate;

import hr.fer.rassus.model.Reading;

public interface RestInterface {
	Double getTemperature();
	Double getHumidity();
	String getConfiguration();
}
