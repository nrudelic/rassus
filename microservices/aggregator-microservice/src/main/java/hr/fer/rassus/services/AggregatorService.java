package hr.fer.rassus.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import hr.fer.rassus.model.Reading;
import hr.fer.rassus.resttemplate.RestInterface;
import hr.fer.rassus.resttemplate.RestTemplateImplementation;

@Service
public class AggregatorService {
	
	private RestInterface restTemp;
	private RestInterface restHum;
	private RestInterface restConfig;
	
	@Autowired
    private org.springframework.cloud.client.discovery.DiscoveryClient discoveryClient;
	
	public AggregatorService() {
		restConfig = new RestTemplateImplementation("http://config:8888");
	}

	public Reading getReadings() {
		

		restTemp = new RestTemplateImplementation(getClientsByApplicationName("temperature-ms"));
		restHum = new RestTemplateImplementation(getClientsByApplicationName("humidity-ms"));
		
		Reading currentReading = new Reading();
		currentReading.setHumidity(Double.parseDouble(restHum.getHumidity().split(">")[1].split("<")[0]));
		currentReading.setTemperature(Double.parseDouble(restTemp.getTemperature().split(">")[1].split("<")[0]));
		
		String config = restConfig.getConfiguration();
		
		String unit = config.split(" ")[1];
		
		switch(unit) {
			case "CELSIUS":
				break;
			case "KELVIN":
				currentReading.setTemperature(currentReading.getTemperature() + 273.15);
				break;
			default:
				break;
		}
		
		return currentReading;
	}
	

	public @ResponseBody String getClientsByApplicationName(@PathVariable String applicationName) {
        return this.discoveryClient.getInstances(applicationName).get(0).getUri().toString();
    }
}
