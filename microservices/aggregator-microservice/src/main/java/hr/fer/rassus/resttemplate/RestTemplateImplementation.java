package hr.fer.rassus.resttemplate;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import hr.fer.rassus.model.Reading;

public class RestTemplateImplementation implements RestInterface{

	private String url;
	private RestTemplate restTemplate;
	
	public RestTemplateImplementation(String url) {
		this.url = url;
		restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	}
	@Override
	public String getTemperature() {
		return restTemplate.getForObject(url + "/current-reading", String.class);
	}

	@Override
	public String getHumidity() {
		return restTemplate.getForObject(url + "/current-reading", String.class);
	}
	
	@Override
	public String getConfiguration() {
		return restTemplate.getForObject(url + "/config-server.properties", String.class);
	}

}
