package hr.fer.rassus.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.rassus.model.Reading;
import hr.fer.rassus.services.AggregatorService;


@RestController
public class AggregatorController {
	private AggregatorService aggregatorService;
	
	public AggregatorController(AggregatorService service) {
		aggregatorService = service;
	}
	
	@GetMapping("/readings")
	public ResponseEntity<Reading> currentReading(){
		return new ResponseEntity<Reading>(aggregatorService.getReadings(), HttpStatus.OK);
	}
}
