package hr.fer.rassus.resttemplate;

import hr.fer.rassus.model.Reading;

public interface RestInterface {
	String getTemperature();
	String getHumidity();
	String getConfiguration();
}
