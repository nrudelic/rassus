package hr.fer.rassus.services;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.rassus.model.Reading;
import hr.fer.rassus.repository.ReadingRepository;
import hr.fer.rassus.resttemplate.RestInterface;
import hr.fer.rassus.resttemplate.RestTemplateImplementation;

@Service
public class TemperatureService {
	@Autowired
	private ReadingRepository readingRepository;
	
	private RestInterface restConfig;
	
	public TemperatureService() {
		restConfig = new RestTemplateImplementation("config:8888");
	}

	public Reading getReading() {
		LocalTime time = LocalTime.now();
		int id = (int) (4 * time.getHour() + time.getMinute() / 15);
		Reading reading = readingRepository.findById(id).get();

		return reading;
	}
}
