package hr.fer.rassus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemperatureMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemperatureMicroserviceApplication.class, args);
		System.out.println("Hej!");
	}

}
