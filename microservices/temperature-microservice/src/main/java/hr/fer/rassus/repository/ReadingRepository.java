package hr.fer.rassus.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import hr.fer.rassus.model.Reading;

public interface ReadingRepository extends JpaRepository<Reading, Integer> {
	public Optional<Reading> findById(Integer id);
}
