package hr.fer.rassus.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import hr.fer.rassus.model.Reading;
import hr.fer.rassus.services.TemperatureService;


@RestController
public class Controller {
	
	private TemperatureService temperatureService;
	
	public Controller(TemperatureService service) {
		temperatureService = service;
	}
	
	@GetMapping("/current-reading")
	public ResponseEntity<Double> currentReading(){
		Double temperature = temperatureService.getReading().getTemperature();
		System.out.println(temperature);
		return new ResponseEntity<Double>(temperature, HttpStatus.OK);
	}
}